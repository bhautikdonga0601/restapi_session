from django.db import models

# Create your models here.
class AuthorDetails(models.Model):
    TYPE_CHOICES = [("Classic", 1), ("StoryTeller", 2)]

    #id = models.AutoField(primary_key=True)
    author_id = models.CharField(max_length=10, unique=True)
    name = models.CharField(max_length=10)
    city = models.CharField(max_length=10)
    address = models.TextField()
    dob = models.DateField(null=True)
    age = models.IntegerField(null=True)
    type = models.CharField(max_length= 15, choices=TYPE_CHOICES)
    weight = models.FloatField()
    email = models.EmailField()

    class Meta:
        db_table = "AUTHOR_DETAILS"
        ordering = ["-age", "weight"]
        unique_together = ["name", "city"]

    def __str__(self):
        return f"{self.author_id} - {self.name}"


class ArticleDetails(models.Model):
    #id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=10)
    date = models.DateField(auto_now_add=True)
    author = models.ForeignKey(to=AuthorDetails, to_field="author_id",
                               on_delete=models.CASCADE, related_name="articles")
