from django.shortcuts import render
from api.models import *
from api.serializers import *
from django.http import JsonResponse
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework import status
from rest_framework import viewsets
from django_filters import rest_framework as filters

# Create your views here.
@api_view(['GET', 'POST'])
def article_list_1(request):

    if request.method == "GET":
        #request.GET.get('age')
        article = ArticleDetails.objects.all()
        data = ArticleDetailsSerializer(article, many=True).data
        return Response(data)

    elif request.method == "POST":
        data = request.data
        serializer = ArticleDetailsSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', "PATCH", 'DELETE'])
def article_list_2(request, pk):

    try:
        article = ArticleDetails.objects.get(pk=pk)
    except:
        Response({"Error": "Object does not exist"}, status=404)

    if request.method == "GET":
        #request.GET.get('age')
        data = ArticleDetailsSerializer(article).data
        return Response(data)

    elif request.method == "PUT":
        data = request.data
        serializer = ArticleDetailsSerializer(article, data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == "PATCH":
        data = request.data
        serializer = ArticleDetailsSerializer(article, data=data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == "DELETE":
        article.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class AuthorDetailsViewSet(viewsets.ModelViewSet):
    queryset = AuthorDetails.objects.all()
    serializer_class = AuthorDetailsSerializer
    lookup_field = "author_id"
    filter_backends = (filters.DjangoFilterBackend, )
    filterset_fields = {
            'age' : ['exact', 'lt', 'iexact', 'lte', 'gt', 'gte'], 
            'weight': ['exact', 'lt'],
            'name': ['icontains']
        } 

