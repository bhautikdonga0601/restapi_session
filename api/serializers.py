from rest_framework import serializers
from api.models import *

class AuthorDetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = AuthorDetails
        fields =  "__all__" # ["name", "city", "dob"]

class ArticleDetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = ArticleDetails
        fields = "__all__"

class ArticleDetailsSerializer2(serializers.ModelSerializer):
    class Meta:
        model = ArticleDetails
        fields = ["title"]
